// void Guardar(List<MiObjeto>() params);
var Controlador = /** @class */ (function () {
    function Controlador(inicial) {
        this.inicial = inicial;
    }
    Object.defineProperty(Controlador.prototype, "nombre", {
        get: function () {
            return "Controlador " + this.inicial;
        },
        enumerable: true,
        configurable: true
    });
    Controlador.prototype.guardar = function (params) {
        var index;
        for (index = 0; index < params.length; index++) {
            console.log(params[index].key + " = " + params[index].value);
        }
    };
    return Controlador;
}());
var miControlador = new Controlador(5);
console.log(miControlador.nombre);
miControlador.guardar([{ key: "10", value: 100 }, { key: "2", value: 200 }]);
