var x = 5;
console.log("El valor de x es: " + x);
// x = "fff";
var str = "hola";
console.log(str);
// str = 5;
var flag = false;
console.log(flag);
var lista = [];
lista.push(5);
console.log("lista: " + lista);
// lista.push("5");
var Intereses;
(function (Intereses) {
    Intereses[Intereses["Aburrido"] = 10] = "Aburrido";
    Intereses[Intereses["Normal"] = 11] = "Normal";
    Intereses[Intereses["Motivado"] = 12] = "Motivado";
})(Intereses || (Intereses = {}));
;
var miInteres = Intereses.Aburrido;
console.log("Me encuentro " + Intereses[miInteres]);
function noRetornaNada() { }
function retornaNumero() {
    return 5;
}
function retornaNumeroParametro(param) {
    return param;
}
// var num = retornaNumeroParametro("5");
var num = retornaNumeroParametro(10);
function retornaCualquierCosa() {
    if (flag)
        return "1";
    return 1;
}
console.log(retornaCualquierCosa());
