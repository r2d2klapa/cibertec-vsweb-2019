var x: number = 5;
console.log(`El valor de x es: ${x}`)
// x = "fff";

let str: string = "hola";
console.log(str);

// str = 5;

var flag: boolean = false;
console.log(flag);

var lista: number[] = [];
lista.push(5);
console.log(`lista: ${lista}`);
// lista.push("5");

enum Intereses { Aburrido = 10, Normal, Motivado };
var miInteres: Intereses = Intereses.Aburrido;
console.log(`Me encuentro ${Intereses[miInteres]}`);

function noRetornaNada(): void { }

function retornaNumero(): number {
    return 5;
}

function retornaNumeroParametro(param: number) {
    return param;
}

// var num = retornaNumeroParametro("5");
var num = retornaNumeroParametro(10);

function retornaCualquierCosa(): any {
    if (flag)
        return "1";
    return 1;
}

console.log(retornaCualquierCosa());