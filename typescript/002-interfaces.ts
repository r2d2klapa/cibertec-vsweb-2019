interface IObjConId {
    id: number,
    nombre: string,
    edad?: number
}

var miObjeto: IObjConId = { id: 1, nombre: "arturo" };

console.log(`Nombre del objeto: ${miObjeto.nombre}`);

function mostrarId(obj: IObjConId):void{
    console.log(`Mostrando el ID: ${obj.id}`)
}

mostrarId(miObjeto);
mostrarId({id: 10, nombre: "juan"});
mostrarId({id: 20, nombre: "jorge", edad: 22} as IObjConId);

