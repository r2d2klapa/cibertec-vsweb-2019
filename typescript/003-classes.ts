interface IControlador {
    nombre: string,
    guardar: (params: {key: string, value: number}[]) => void
}

// void Guardar(List<MiObjeto>() params);

class Controlador implements IControlador{
    constructor(private inicial:number){}
    get nombre():string{
        return `Controlador ${this.inicial}`
    }

    guardar(params:{key:string, value:number}[]):void{
        var index: number;
        for(index=0;index<params.length;index++){
            console.log(`${params[index].key} = ${params[index].value}`);
        }
    }
}

var miControlador = new Controlador(5);
console.log(miControlador.nombre);
miControlador.guardar([{key:"10", value:100},{key:"2", value:200}]);