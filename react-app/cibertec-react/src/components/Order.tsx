import * as React from "react";
import {selectItem} from "../types/base";
import { API_ENDPOINT } from "../variables/global";
import axios from "axios";

export const Order2 = () =>{

}

interface IProps {
    customerItems: selectItem[],
    updateOrderId: any
}

interface IOrder {
    id: number;
    customerId: number;
    orderNumber: string;
}

const Order : React.FC<IProps> = (props)=>{
    // const [numeroOrden, setNumeroOrden] = React.useState<string>("");
    // const [customerId, setCustomerId] = React.useState<string>("");

    const [order, setOrder] = React.useState<IOrder>({
        id: 0,
        customerId: 0, // incializar con 0
        orderNumber: ""
    })
    
    
    // declarar una función que realice el post para insertar o actualizar la orden
    const guardarOrden = () => {
        let objetoGuardar = {
            id: order.id,
            customerId: order.customerId            
        }
        
        // llamar al servicio del api para guardar la orden
        axios.post(`${API_ENDPOINT}/Order`,objetoGuardar)
        .then(response=>{
            console.log("Resultado de guardar orden: ", response);
            setOrder({...order, id: response.data.Id, orderNumber: response.data.OrderNumber});
            // actualizamos el orderId del componente order items
            props.updateOrderId(response.data.Id);
        })
    }

    const handleCustomerChange = (e : React.ChangeEvent<any>) =>{
        console.log(e.target.value);
        // cada vez que se selecciona un customer diferente, asignamos el valor al state correspondiente
        // setCustomerId(e.target.value);
        var custId = Number(e.target.value);
        setOrder({...order, customerId: custId});        
    }

    return (
        <React.Fragment>
        <div className="row">
            <div className="col">
                <h2>Orden N° {order.orderNumber}</h2>
            </div>
        </div>
        <div className="row">
            <div className="col">
                <label htmlFor="customer">Seleccione el Cliente:</label>
                <select className="form-control" name="customer" id="customer" onChange={handleCustomerChange}>
                {
                    // recorrer el array y pintar cada <option> con sus respectivos valores
                    props.customerItems.map((item,index)=>{
                        return (
                            <option key={index} value={item.value}>{item.text}</option>
                        )
                    })
                }
                </select>
            </div>
        </div>
        <div className="row">
            <div className="col">
                <button className="btn btn-primary" onClick={()=>{guardarOrden()}}>Guardar Orden</button>
            </div>
        </div>
        </React.Fragment>
    )
}

export default Order;
