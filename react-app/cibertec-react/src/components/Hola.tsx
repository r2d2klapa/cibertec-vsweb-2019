import * as React from "react";

interface IProps {
    name?: string;
}

const Hola : React.FC<IProps> = (props) => {
    // usando react hooks
    const [contador,setContador] = React.useState<number>(0);

    const incrementarContador = () => {
        setContador(contador+1);
    }

    const decrementarContador = () => {
        setContador(contador-1);
    }

    return (
        <React.Fragment>
        <div>
            <p>
                Hola {props.name} desde el componente
            </p>
            <button onClick={()=>{ incrementarContador() }}>+</button>
            <button onClick={()=>{ decrementarContador() }}>-</button>
            <span>{contador}</span>
        </div>
        {/* <button></button> */}
        </React.Fragment>
    )
}

export default Hola;