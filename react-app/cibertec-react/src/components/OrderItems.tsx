import * as React from "react";
import { selectItem } from 'src/types/base';
import axios from "axios";
import { API_ENDPOINT } from 'src/variables/global';

interface IProps {
    orderId: number
}

interface IOrderItem {
    orderId: number,
    productId: number,
    quantity: number
}

const OrderItems : React.FC<IProps> = (props) =>{
    const [products, setProducts] = React.useState<selectItem[]>([]);
    const [orderItem, setOrderItem] = React.useState<IOrderItem>({
        orderId: 0,
        productId: 0,
        quantity:0
    });

    React.useEffect(()=>{
        // este código solo se ejecutará la primera vez, ya que le estamos enviando un arreglo vacío como segundo parámetro
        axios.get(`${API_ENDPOINT}/product`)
        .then(response=>{
            let listaProductos : selectItem[] = [];
            response.data.map((product : any)=>{
                listaProductos.push({text: product.ProductName, value: product.Id})
            })
            setProducts(listaProductos);
            console.log("Lista Productos", listaProductos);
        })

        // asignar el valor del orderId que obtenemos desde las props
        setOrderItem({...orderItem, orderId: props.orderId});
    },[props.orderId])

    const onInputChange = (e: React.ChangeEvent<any>)=>{
        let name = e.target.name;
        let value = e.target.value;
        setOrderItem({...orderItem,[name]: value});
    }

    const guardarOrderItem = ()=>{
        axios.post(`${API_ENDPOINT}/orderitem`,orderItem)
        .then(response=>{
            console.log("Resultado de guardar Order Item: ", response);
        })
    }

    return(
        <React.Fragment>
            <div className="row">
                <div className="col">
                    <h2>Order Items</h2>
                    <small>Order ID: {props.orderId}</small>
                    <br/>
                    <small>Order ID (state): {orderItem.orderId}</small>
                    <br/>
                    <small>ProductId: {orderItem.productId}</small>
                    <br/>
                    <small>Quantity: {orderItem.quantity}</small>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label htmlFor="products">Elige un producto</label>
                    <select name="productId" id="products" className="form-control" onChange={onInputChange}>
                    {products.map((item,index)=>{
                        return (
                            <option key={index} value={item.value}>{item.text}</option>
                        )
                    })}
                    </select>
                    <label htmlFor="cantidad">Ingresa la cantidad</label>
                    <input type="text" name="quantity" className="form-control" onChange={onInputChange}/>
                    <button className="btn btn-primary btn-xs" onClick={()=>{guardarOrderItem()}}>Agregar Item</button>
                </div>
            </div>
        </React.Fragment>
    )
}

export default OrderItems