import * as React from "react"
import Order from "./components/Order"
import OrderItems from "./components/OrderItems"
import { selectItem } from "./types/base"
import { API_ENDPOINT } from "./variables/global"

// importar la librería axios que sirve para realizar request HTTP
import axios from "axios"

const AppOrderItem : React.FC = ()=>{
    const [customers, setCustomers] = React.useState<selectItem[]>([]);
    const [orderId, setOrderId] = React.useState<number>(0);

    React.useEffect(()=>{
        // obtener la data del api
        axios.get(`${API_ENDPOINT}/customer`)
        .then((response)=>{
            console.log(response.data);
            let customersResponse: selectItem[] = []
            // agregamos la data al arreglo del tipo selectItem[]
            response.data.map((item : any)=>{
                customersResponse.push({value:item.Id, text:item.FirstName})
            })

            // asignamos el nuevo arreglo al state para pasarlo al componente
            setCustomers(customersResponse)
        })
    }, []);
    
    const updateOrderIdFromOrder = (newOrderId:number)=>{
        setOrderId(newOrderId);
    }
    return (
        <div className="container">
            <button onClick={()=>{setOrderId(orderId+1)}}>Incrementar Order ID</button>
            <Order customerItems={customers} updateOrderId={updateOrderIdFromOrder}></Order>
            <OrderItems orderId={orderId}></OrderItems>
        </div>        
    )
}

export default AppOrderItem;