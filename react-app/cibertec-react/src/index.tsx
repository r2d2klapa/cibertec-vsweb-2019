import * as React from 'react';
import * as ReactDOM from 'react-dom';
// import App2 from './App';
// import Hola from './components/Hola'
import AppOrderItems from "./AppOrderItems";
import './index.css';
import registerServiceWorker from './registerServiceWorker';
// para importar BS
import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  // <Hola name="Arturo" />,
  <AppOrderItems></AppOrderItems>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
