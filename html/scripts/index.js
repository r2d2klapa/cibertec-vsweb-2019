﻿(function (cibertec) {
    cibertec.Index = {
        currentYear: function () {
            // obtener el año actual
            var fecha = new Date();
            return fecha.getFullYear();
        }
    };
})(window.cibertec = window.cibertec || {});
