﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Cibertec.UnitOfWork;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.Models;
using System.Linq;

namespace Cibertec.Data.Tests
{
    /// <summary>
    /// Descripción resumida de CustomerRepositoryTests
    /// </summary>
    [TestClass]
    public class CustomerRepositoryTests
    {
        private readonly IUnitOfWork unitOfWork;

        public CustomerRepositoryTests()
        {
            // preparación o configración
            unitOfWork = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ConnectionString);
        }

        [TestMethod]
        public void ObtenerPorNombreTest()
        {
            var resultado = unitOfWork.Customers.ObtenerCustomerPorNombre("Maria");
            Assert.AreEqual(2, resultado.Count);
        }

        [TestMethod]
        public void ObtenerTodoTest()
        {
            // ejecución: obtener data
            var data = unitOfWork.Customers.GetList();
            // comprobación
            Assert.AreEqual(true, data.Count > 0);
        }

        [TestMethod]
        public void ActualizarCustomerTest()
        {
            // ejecución: obtener data
            // 1. Obtener el objeto de BD que se va a actualizar
            var objetoActualizar = unitOfWork.Customers.Get(1);

            // 2. Hacemos los cambios de las propiedades que queremos actualizar
            objetoActualizar.FirstName = "Maria";
            objetoActualizar.LastName = "Anders";

            var resultado = unitOfWork.Customers.Update(objetoActualizar);

            // comprobación
            // 1. Obtener el objeto de BD nuevamente
            var objetoBD = unitOfWork.Customers.Get(1);
            Assert.AreEqual("Maria", objetoBD.FirstName);
        }

        [TestMethod]
        public void InsertarCustomerTest()
        {
            var objetoInsertar = new Customer
            {
                FirstName = "Arturo",
                LastName = "Balbin"
            };

            var nuevoId = unitOfWork.Customers.Insert(objetoInsertar);

            // obtener el objeto insertado para hacer la comprobación
            var objetoBD = unitOfWork.Customers.Get((int)nuevoId);

            Assert.AreEqual(objetoInsertar.FirstName, objetoBD.FirstName);
        }

        [TestMethod]
        public void EliminarCustomerTest()
        {
            // obtener la lista de customers
            var listaCustomers = unitOfWork.Customers.GetList();

            // buscamos el registro que tenga los datos de la isnerción anterior
            var arturo = listaCustomers.FirstOrDefault(c => c.FirstName == "Arturo" && c.LastName == "Balbin");

            // borramos el registro
            var resultadoEliminar = unitOfWork.Customers.Delete(arturo);

            // si ya lo eliminé, esto debería ser null
            var objetoArturoBD = unitOfWork.Customers.Get(arturo.Id);

            Assert.AreEqual(null, objetoArturoBD);
        }
    }
}
