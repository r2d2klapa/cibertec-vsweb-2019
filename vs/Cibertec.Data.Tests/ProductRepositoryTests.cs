﻿using System;
using System.Configuration;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.UnitOfWork;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cibertec.Models;
using System.Linq;

namespace Cibertec.Data.Tests
{
    [TestClass]
    public class ProductRepositoryTests
    {
        private readonly IUnitOfWork unitOfWork;

        public ProductRepositoryTests()
        {
            unitOfWork = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ConnectionString);
        }

        [TestMethod]
        public void ObtenerReporteProductosMasVendidosTest()
        {
            var inicio = new DateTime(2013, 1, 1);
            var fin = new DateTime(2014, 4, 1);
            
            var resultado = unitOfWork.Products.ObtenerReporteProductosMasVendidos(inicio, fin);

            Assert.AreEqual("Gnocchi di nonna Alice", resultado.First().ProductName);
        }
    }
}
