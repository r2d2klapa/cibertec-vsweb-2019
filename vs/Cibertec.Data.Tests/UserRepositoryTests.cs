﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cibertec.Models;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.UnitOfWork;
using System.Configuration;

namespace Cibertec.Data.Tests
{
    [TestClass]
    public class UserRepositoryTests
    {
        private readonly IUnitOfWork unitOfWork;
        public UserRepositoryTests()
        {
            unitOfWork = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ConnectionString);
        }
        [TestMethod]
        public void ValidateUserTest()
        {
            var usuarioCorreo = unitOfWork.Users.ValidateUser("correo@mail.com", "123456");
            Assert.AreEqual("Correo", usuarioCorreo.FirstName);
        }
    }
}
