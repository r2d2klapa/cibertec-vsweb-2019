﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cibertec.UnitOfWork;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.Models;
using System.Configuration;
using System.Linq;

namespace Cibertec.Data.Tests
{
    [TestClass]
    public class OrderRepositoryTests
    {
        private readonly IUnitOfWork unitOfWork;
        public OrderRepositoryTests()
        {
            unitOfWork = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ConnectionString);
        }
        [TestMethod]
        public void ReporteGerenciaTest()
        {
            var reporte = unitOfWork.Orders.ObtenerReporteGerencia();
            Assert.AreEqual(false, reporte.Any(r => r.TotalAmount < 2000));
            Assert.AreEqual(true, reporte.Count > 0);
        }

        [TestMethod]
        public void ObtenerUltimoNumeroOrden()
        {
            var numeroOrden = unitOfWork.Orders.ObtenerUltimoNumeroDeOrden();
            Assert.AreEqual(true, !string.IsNullOrEmpty(numeroOrden));
        }

        [TestMethod]
        public void ObtenerOrderItemsNombreProducto()
        {
            var resultado = unitOfWork.OrderItems.ObtenerListaNombreProducto(1);
            Assert.AreEqual(3, resultado.Count());
        }
    }
}
