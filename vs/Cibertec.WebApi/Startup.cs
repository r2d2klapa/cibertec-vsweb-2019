﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(Cibertec.WebApi.Startup))]

namespace Cibertec.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // configración de las rutas
            RouteConfig.Register(config);

            DIConfig.ConfigureInjector(config);

            app.UseCors(CorsOptions.AllowAll);

            app.UseWebApi(config);
        }
    }
}
