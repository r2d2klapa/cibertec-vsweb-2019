﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Cibertec.WebApi
{
    public static class RouteConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // para las rutas que se definen como atributos de las acciones de los controladores
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                    name: "DefaultWebApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //        name: "DefaultWebApi2",
            //        routeTemplate: "api/v2/{controller}/{id}",
            //        defaults: new { id = RouteParameter.Optional }
            //);

            //config.Routes.MapHttpRoute(
            //        name: "DefaultWebApi3",
            //        routeTemplate: "api/v3/{controller}/{id}",
            //        defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}