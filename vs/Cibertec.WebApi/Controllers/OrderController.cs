﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cibertec.Models;
using Cibertec.UnitOfWork;

namespace Cibertec.WebApi.Controllers
{
    public class OrderController : BaseController
    {
        public OrderController(IUnitOfWork unit) : base(unit)
        {
        }

        [HttpPost]
        public IHttpActionResult Post(Order order)
        {
            int resultadoInsert = 0;
            bool resultadoUpdate = false;
            bool esActualizacion = order.Id > 0;

            var resultado = new Order();

            if (!esActualizacion)
            {
                order.OrderDate = DateTime.Now;

                // asignar el numero de orden
                var ultimoNumeroOrden = Convert.ToInt64(_unit.Orders.ObtenerUltimoNumeroDeOrden());
                order.OrderNumber = $"{ultimoNumeroOrden + 1}";

                resultadoInsert = (int)_unit.Orders.Insert(order);
                resultado = _unit.Orders.Get(resultadoInsert);
            }
            else
            {
                var orderBd = _unit.Orders.Get(order.Id);
                order.OrderDate = orderBd.OrderDate;
                order.OrderNumber = orderBd.OrderNumber;

                // al actualizar devolver el objeto actualizado
                resultadoUpdate = _unit.Orders.Update(order);
                resultado = _unit.Orders.Get(order.Id);
            }

            return Ok(resultado);
        }
    }
}
