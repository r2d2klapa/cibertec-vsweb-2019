﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Cibertec.WebApi.Models;

namespace Cibertec.WebApi.Controllers
{
    public class ProductoDemoController : ApiController
    {
        List<ProductoDemo> productos = new List<ProductoDemo>
        {
            new ProductoDemo { Id = 1, Name = "Teclado", Category="Cómputo", Price = 50 },
            new ProductoDemo { Id = 2, Name = "Pantalla", Category="Cómputo", Price = 500 },
            new ProductoDemo { Id = 3, Name = "Mouse", Category="Cómputo", Price = 40 },
        };

        public IEnumerable<ProductoDemo> GetTodos()
        {
            return productos;
        }

        public IHttpActionResult GetProducto(int id)
        {
            var producto = productos.FirstOrDefault(p => p.Id == id);
            if (producto == null) return NotFound();
            return Ok(producto);
        }
    }
}