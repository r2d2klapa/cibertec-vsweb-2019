﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cibertec.Models;
using Cibertec.Models.ViewModels;
using Cibertec.UnitOfWork;

namespace Cibertec.WebApi.Controllers
{
    public class OrderItemController : BaseController
    {
        public OrderItemController(IUnitOfWork unit) : base(unit)
        {
        }

        [Route("api/orderitems/productnames/{orderId}")]
        public IEnumerable<OrderItemViewModel> GetWithProductNames(int orderId)
        {
            return _unit.OrderItems.ObtenerListaNombreProducto(orderId);
        }

        public IHttpActionResult Post(OrderItem orderItem)
        {
            // obtener la información del producto
            var product = _unit.Products.Get(orderItem.ProductId);
            orderItem.UnitPrice = product.UnitPrice.HasValue ? product.UnitPrice.Value : 0M;
            var resultado = _unit.OrderItems.Insert(orderItem);
            if (resultado > 0)
            {
                // insertó el item correctamente
                // ahora tenemos que actualizar el total de la orden
                var order = _unit.Orders.Get(orderItem.OrderId);

                // calcular el nuevo monto total de la orden
                order.TotalAmount += (orderItem.UnitPrice * orderItem.Quantity);
                var resultadoUpdate = _unit.Orders.Update(order);
                order = _unit.Orders.Get(orderItem.OrderId);
                return Ok(order);
            }
            return InternalServerError(new Exception("Error en la inserción del item"));
        }

        public IHttpActionResult Delete(int id)
        {
            var orderItem = _unit.OrderItems.Get(id);
            var order = _unit.Orders.Get(orderItem.OrderId);
            // acutalizar el monto total de la orden
            order.TotalAmount -= (orderItem.UnitPrice * orderItem.Quantity);
            // actualizar en BD
            var resultadoOrder = _unit.Orders.Update(order);
            // eliminar el item
            var resultado = _unit.OrderItems.Delete(orderItem);
            return Ok(order);
        }
    }
}
