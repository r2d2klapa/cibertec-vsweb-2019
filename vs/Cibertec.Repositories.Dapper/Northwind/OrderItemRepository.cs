﻿using Cibertec.Models;
using Cibertec.Models.ViewModels;
using Cibertec.Repositories.Northwind;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class OrderItemRepository : Repository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<OrderItemViewModel> ObtenerListaNombreProducto(int orderId)
        {
            var sql = @"select oi.Id, oi.UnitPrice, oi.Quantity, p.ProductName
                        from OrderItem oi left join Product p
                        on oi.ProductId = p.Id
                        where oi.OrderId = @orderId";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<OrderItemViewModel>(sql, new { orderId });
            }
        }
    }
}
