﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using System.Data.SqlClient;
using Dapper;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// Método que devuelve la cantidad de registros de la tabla
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                return conn.ExecuteScalar<int>("select count(1) from Customer");
            }
        }

        public List<Customer> ObtenerCustomerPorNombre(string nombre)
        {
            var sql = @"select * from Customer where FirstName = @FirstNameParametro";
            //var sql = @"select * from Customer where FirstName = @FirstName and LastName = @LastName";
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new { FirstNameParametro = nombre };
                //var customer = new Customer { FirstName = "Maria" };
                //var parameters = new { FirstName = "Maria", LastName = "Apellido" };
                return connection.Query<Customer>(sql, parameters).ToList();
            }
        }

        public List<Customer> PagedList(int startRow, int endRow)
        {
            if (startRow > endRow) return new List<Customer>();
            using (var conn = new SqlConnection(_connectionString))
            {
                return conn.Query<Customer>("CustomerPagedList", new { startRow, endRow }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }
    }
}
