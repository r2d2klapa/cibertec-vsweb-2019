﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.UnitOfWork;
using Cibertec.Models;
using Cibertec.Repositories.Northwind;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class NorthwindUnitOfWork : IUnitOfWork
    {
        public ICustomerRepository Customers { get; private set; }
        public IOrderRepository Orders { get; private set; }
        public IProductRepository Products { get; private set; }
        public IUserRepository Users { get; private set; }
        public IRepository<Supplier> Suppliers { get; private set; }
        public IOrderItemRepository OrderItems { get; private set; }

        public NorthwindUnitOfWork(string connectionString)
        {
            Customers = new CustomerRepository(connectionString);
            Orders = new OrderRepository(connectionString);
            Products = new ProductRepository(connectionString);
            Users = new UserRepository(connectionString);
            Suppliers = new Repository<Supplier>(connectionString);
            OrderItems = new OrderItemRepository(connectionString);
        }
    }
}
