﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;
using Cibertec.Repositories.Northwind;
using System.Data.SqlClient;
using Dapper;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(string connectionString) : base(connectionString)
        {
        }

        public List<OrderReport> ObtenerReporteGerencia()
        {
            var sql = @"
                        select o.Id as OrderId, c.FirstName, c.LastName, o.TotalAmount 
                        from [Order] o
                        left join Customer c on o.CustomerId = c.Id
                        where o.TotalAmount >= 2000
                        order by o.TotalAmount
                       ";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<OrderReport>(sql).ToList();
            }
        }

        public string ObtenerUltimoNumeroDeOrden()
        {
            var sql = @"select max(OrderNumber) from [Order]";
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryFirstOrDefault<string>(sql);
            }
        }
    }
}
