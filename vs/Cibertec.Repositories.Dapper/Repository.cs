﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;

namespace Cibertec.Repositories.Dapper
{
    public class Repository<Entity> : IRepository<Entity> where Entity : class
    {
        protected readonly string _connectionString;
        public Repository(string connectionString)
        {
            _connectionString = connectionString;
            SqlMapperExtensions.TableNameMapper = (type) => { return $"[{type.Name}]"; };
        }

        /// <summary>
        /// Este método devuelve todos los registros de la tabla
        /// </summary>
        /// <returns></returns>
        public List<Entity> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Entity>().ToList();
            }
        }

        public async Task<List<Entity>> GetListAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lista = await connection.GetAllAsync<Entity>();
                return lista.ToList();
            }
        }

        /// <summary>
        /// Devuelve el registro en base al id enviado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity Get(long id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Get<Entity>(id);
            }
        }

        /// <summary>
        /// Método para insertar un nuevo registro
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Devuelve el ID del registro insertado</returns>
        public long Insert(Entity entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Insert(entity);
            }
        }

        /// <summary>
        /// Este método sirve para actualizar un registro en BD
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Update(Entity entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Update(entity);
            }
        }

        /// <summary>
        /// Elimina el registro enviado de la tabla
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Delete(Entity entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Delete(entity);
            }
        }
    }
}
