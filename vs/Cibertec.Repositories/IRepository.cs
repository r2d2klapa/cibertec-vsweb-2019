﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories
{
    public interface IRepository<Entity> where Entity : class
    {
        Entity Get(long id);
        List<Entity> GetList();
        long Insert(Entity entity);
        bool Update(Entity entity);
        bool Delete(Entity entity);
    }
}
