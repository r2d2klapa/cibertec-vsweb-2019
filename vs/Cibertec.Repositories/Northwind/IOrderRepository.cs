﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Northwind
{
    public interface IOrderRepository : IRepository<Order>
    {
        List<OrderReport> ObtenerReporteGerencia();
        string ObtenerUltimoNumeroDeOrden();
    }
}
