﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Northwind
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        List<Customer> ObtenerCustomerPorNombre(string nombre);
        List<Customer> PagedList(int startRow, int endRow);
        int Count();
    }
}
