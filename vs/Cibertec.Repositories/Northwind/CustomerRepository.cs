﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Northwind
{
    public class CustomerRepository
    {
        private readonly string _connectionString;

        public CustomerRepository(string connectionString)
        {
            _connectionString = connectionString;
            SqlMapperExtensions.TableNameMapper = (type) => { return $"{type.Name}"; };
        }

        

    }
}
