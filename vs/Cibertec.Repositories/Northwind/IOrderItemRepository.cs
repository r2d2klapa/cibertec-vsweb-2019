﻿using Cibertec.Models;
using Cibertec.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cibertec.Repositories.Northwind
{
    public interface IOrderItemRepository : IRepository<OrderItem>
    {
        IEnumerable<OrderItemViewModel> ObtenerListaNombreProducto(int orderId);
    }
}
