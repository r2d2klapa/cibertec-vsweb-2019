﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Repositories;
using Cibertec.Repositories.Northwind;
using Cibertec.Models;

namespace Cibertec.UnitOfWork
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customers { get; }
        IOrderRepository Orders { get; }
        IProductRepository Products { get; }
        IUserRepository Users { get; }
        IRepository<Supplier> Suppliers { get; }
        IOrderItemRepository OrderItems { get; }
    }
}