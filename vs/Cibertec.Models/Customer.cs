﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Cibertec.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Display(Name = "Nombres")]
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string FirstName { get; set; }

        [Display(Name = "Apellidos")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "Ciudad")]
        public string City { get; set; }

        [Display(Name = "País")]
        public string Country { get; set; }

        [Display(Name = "Teléfono")]
        [MaxLength(20, ErrorMessage = "Por favor, ingrese un texto con un máximo de 20 caracteres")]
        public string Phone { get; set; }
    }
}
