﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;

[assembly: OwinStartup(typeof(Cibertec.Mvc.Startup))]

namespace Cibertec.Mvc
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Para obtener más información sobre cómo configurar la aplicación, visite https://go.microsoft.com/fwlink/?LinkID=316888

            // configurar la autenticación del aplicativo
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                CookieName = "CibertecAuth",
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Account/Login")
            });

            // inicializar hubs de SignalR
            app.MapSignalR();
        }
    }
}
