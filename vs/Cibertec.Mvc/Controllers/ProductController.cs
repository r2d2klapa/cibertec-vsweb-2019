﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibertec.Models;
using Cibertec.UnitOfWork;

namespace Cibertec.Mvc.Controllers
{
    public class ProductController : BaseController
    {
        public ProductController(IUnitOfWork unit) : base(unit)
        {
        }

        // GET: Product
        public ActionResult Index(string query = "")
        {
            var data = _unitOfWork.Products.GetList();
            if (query != "") data = data.Where(p => p.ProductName.ToUpper().Contains(query.ToUpper())).ToList();
            return View(data);
        }

        public ActionResult Create()
        {
            ViewBag.Suppliers = new SelectList(_unitOfWork.Suppliers.GetList(), "Id", "CompanyName");
            return PartialView("_Create", new Product());
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            if (!ModelState.IsValid) return View(product);
            var resultado = _unitOfWork.Products.Insert(product);
            return RedirectToAction("Index");
        }
    }
}