﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibertec.Models;
using Cibertec.Repositories.Dapper.Northwind;
using Cibertec.UnitOfWork;
using System.Configuration;
using log4net;
using Cibertec.Mvc.ActionFilters;

namespace Cibertec.Mvc.Controllers
{
    [LogActionFilter(ParametroFiltro = "Adicional Customer")]
    public class CustomerController : BaseController
    {
        public CustomerController(IUnitOfWork unit) : base(unit)
        {
        }

        // GET: Customer
        [Route("VerCustomers")]
        [Route("Customer/Index/{query}")]
        [Route("Customer")]
        public ActionResult Index(string query = "")
        {
            // escribir un mensaje en el log
            //var log = LogManager.GetLogger(typeof(CustomerController));
            //log.Info("Visitó la acción Index del controlador Customer");



            var data = _unitOfWork.Customers.GetList();

            // para buscar
            if (query != "")
                data = data.Where(c => c.FirstName.Contains(query)).ToList();

            // ahora vamos a pasar la data como parte del modelo de la vista
            return View(data);
        }

        [Authorize(Roles = "admin")]
        //[Authorize(Roles = "admin,admin2,admin3")]
        public ActionResult Create()
        {
            return PartialView("_Create", new Customer());
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Create(Customer customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var resultado = _unitOfWork.Customers.Insert(customer);

                    // si se insertó correctamente, redireccionar al listado
                    if (resultado > 0) return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return PartialView("_Create", customer);
        }

        public ActionResult Edit(int id)
        {
            var model = _unitOfWork.Customers.Get(id);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(int id, Customer customer)
        {
            if (id != customer.Id)
            {
                ModelState.AddModelError("", "IDs inválidos");
            }
            else
            {
                var resultado = _unitOfWork.Customers.Update(customer);
                if (resultado) return RedirectToAction("Index");
            }
            return View(customer);
        }

        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", _unitOfWork.Customers.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(Customer customer)
        {
            if (_unitOfWork.Customers.Delete(customer)) return RedirectToAction("Index");
            return View(customer);
        }

        [Route("Customer/NumeroPaginas/{tamanioPagina}")]
        public int NumeroPaginas(int tamanioPagina)
        {
            var total = _unitOfWork.Customers.Count();
            return total % tamanioPagina == 0 ? total / tamanioPagina : total / tamanioPagina + 1;
        }

        [Route("Customer/List/{pagina}/{tamanioPagina}")]
        public PartialViewResult List(int pagina, int tamanioPagina)
        {
            if (pagina <= 0 || tamanioPagina <= 0) return PartialView("_List", new List<Customer>());

            // calcular los valores de inicio y fin
            var filaInicio = ((pagina - 1) * tamanioPagina) + 1;
            var filaFin = pagina * tamanioPagina;

            var data = _unitOfWork.Customers.PagedList(filaInicio, filaFin);
            return PartialView("_List", data);
        }
    }
}