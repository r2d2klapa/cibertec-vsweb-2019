﻿using Cibertec.Mvc.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.Mvc.Controllers
{
    //[LogActionFilter]
    public class PruebaController : Controller
    {
        // GET: Prueba
        public string Index()
        {
            return "Hola desde el controlador <b>Prueba</b>";
        }

        public string GenerarError()
        {
            throw new Exception("Error Generado desde el controlador Prueba");
        }

        // Ruta: Prueba/Bienvenido/{nombre}/{veces}
        // Ejemplo: Prueba/Bienvenido/Arturo/3
        public ActionResult Bienvenido(string nombre, int veces)
        {
            //var nombre = "Arturo";
            //return $"Hola {nombre} desde el controlador <b>Prueba</b>. Número de veces: {veces}";

            ViewBag.Nombre = nombre;
            ViewBag.Veces = veces;

            return View();
        }

        public ActionResult OrderItems()
        {
            return View();
        }
    }
}