﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibertec.UnitOfWork;
using Cibertec.Mvc.Models;
using System.Security.Claims;

namespace Cibertec.Mvc.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(IUnitOfWork unit) : base(unit)
        {
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View(new UserViewModel { ReturnUrl = returnUrl });
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(UserViewModel user)
        {
            if (!ModelState.IsValid) return View(user);
            var usuarioValido = _unitOfWork.Users.ValidateUser(user.Email, user.Password);
            if (usuarioValido == null)
            {
                ModelState.AddModelError("", "Usuario o contraseña inválidos");
                return View(user);
            }

            // si llegamos a esta línea significa que el usuario ha sido validado
            // asignamos los valores que se guardarán en la cookie
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Email,usuarioValido.Email),
                new Claim(ClaimTypes.Role, usuarioValido.Roles),
                new Claim(ClaimTypes.Name, $"{usuarioValido.FirstName} {usuarioValido.LastName}"),
                new Claim(ClaimTypes.NameIdentifier, usuarioValido.Id.ToString()),
            }, "ApplicationCookie");

            // iniciamos sesión con el objeto respectivo del contexto de owin
            var authManager = Request.GetOwinContext().Authentication;
            authManager.SignIn(identity);

            // redireccionar la URL respectiva
            return RedirectToLocal(user.ReturnUrl);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            // obtenemos el contexto de autenticación
            var authManager = Request.GetOwinContext().Authentication;
            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Account");
        }
    }
}