﻿(function (product) {
    product.tamanioPagina = 10;
    product.cantidadPaginas = 1;
    // para actualizar la tabla luego de una actualización
    product.paginaActual = 1;
    product.success = success;

    // inicializar();

    return product;

    function success(option) {
        // actualizar la página seleccionada
        // getCustomers(customer.paginaActual);

        // cerrar el modal
        cibertec.closeModal(option);
    }

    function inicializar() {
        // se inicializará la lista de customers con su paginación
        // Primer paso: obtener la cantidad total de registros
        $.get(`/Customer/NumeroPaginas/${customer.tamanioPagina}`, function (data) {
            // asignamos el valor de la cantidad de páginas en base al tamaño de página
            customer.cantidadPaginas = data;

            // Segundo paso: inicializar el plugin BootPag
            $(".pagination").bootpag({
                total: customer.cantidadPaginas,
                page: 1,
                maxVisible: 5,
                leaps: true,
                firstLastUse: true,
                first: '←',
                last: '→',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            }).on('page', function (event, num) {
                // lo que se hará cada vez que se cambia de página
                customer.paginaActual = num;
                getCustomers(num);
            });

            // al inicializar siempre mostrar la data de la primera página
            getCustomers(1);
        })
    }

    function getCustomers(numeroPagina) {
        var url = `/customer/list/${numeroPagina}/${customer.tamanioPagina}`;
        $.get(url, function (data) {
            // pintar los resultados en el html
            $(".content").html(data);
        })
    }


})(window.product = window.product || {})