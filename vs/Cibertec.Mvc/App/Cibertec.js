﻿(function (cibertec) {
    function getModalContent(url) {
        // invocar por AJAX a la acción que nos devuelva la vista parcial correspondiente
        $.get(url, function (data) {
            $(".modal-body").html(data);
        })
    }

    function closeModal(option) {
        // cerrar el modal haciendo clic en el botón cerrar
        $("button[data-dismiss='modal']").click();

        // limpiar el contenido modal
        $(".modal-body").html("");

        // mostrar la alerta
        mostrarAlertas(option);
    }

    function mostrarAlertas(option) {
        // esconder todas las alertas
        //$("#nm").addClass("hidden");
        //$("#em").addClass("hidden");
        //$("#emm").addClass("hidden");
        var mensaje = "";
        var titulo = "";
        switch (option) {
            case "create":
                //$("#nm").removeClass("hidden");
                mensaje = "Se registró la información correctamente."
                titulo = "Registro";
                break;
            case "edit":
                //$("#em").removeClass("hidden");
                mensaje = "Se editó la información correctamente.";
                titulo = "Edición";
                break;
            case "delete":
                //$("#emm").removeClass("hidden");
                mensaje = "Se eliminó la información correctamente."
                titulo = "Eliminación";
                break;
            default: break;
        }

        toastr.options.closeButton = true;
        toastr.success(mensaje, titulo);

    }

    // crear los métodos globales para el namespace cibertec
    cibertec.getModal = getModalContent;
    cibertec.closeModal = closeModal;

})(window.cibertec = window.cibertec || {})