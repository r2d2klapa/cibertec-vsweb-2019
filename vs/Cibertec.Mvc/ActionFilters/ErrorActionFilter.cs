﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace Cibertec.Mvc.ActionFilters
{
    public class ErrorActionFilter : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var log = LogManager.GetLogger(typeof(ErrorActionFilter));

            // indicar que la excepción ha sido controlada
            filterContext.ExceptionHandled = true;
            // escribir el mensaje de error
            log.Error(filterContext.Exception);
            // redireccionar a una pantalla genérica de error
            filterContext.Result = new ViewResult() { ViewName = "Error" };
        }
    }
}