﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace Cibertec.Mvc.ActionFilters
{
    public class LogActionFilter : ActionFilterAttribute, IActionFilter
    {
        public string ParametroFiltro { get; set; }

        // este método se invoca antes de ejecutar la acción del controlador
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            // obtenemos los datos para escribir el mensaje en el log
            var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var controllerType = filterContext.ActionDescriptor.ControllerDescriptor.ControllerType;
            var action = filterContext.ActionDescriptor.ActionName;

            // esribir el log
            var log = LogManager.GetLogger(controllerType);
            log.Info($"Action: {action} - Controller: {controller} - Param: {ParametroFiltro}");
        }
    }
}