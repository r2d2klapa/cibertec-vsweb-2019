﻿using Cibertec.Mvc.ActionFilters;
using System.Web;
using System.Web.Mvc;

namespace Cibertec.Mvc
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            // agregamos el filtro personalizado
            filters.Add(new LogActionFilter());
            filters.Add(new ErrorActionFilter());
        }
    }
}
