﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Cibertec.UnitOfWork;
using Cibertec.Repositories.Dapper.Northwind;
using System.Configuration;
using System.Reflection;
using System.Web.Mvc;

namespace Cibertec.Mvc
{
    public class DIConfig
    {
        public static void ConfigureInjector()
        {
            // declaramos el IoC Container
            var container = new Container();

            // todas las instancias del IoC se van a re crear por cada request al servidor
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // registrar nuestras dependencias
            container.Register<IUnitOfWork>(() => new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthwindConnection"].ConnectionString));

            // registrar las dependencias para todos los controladores
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}