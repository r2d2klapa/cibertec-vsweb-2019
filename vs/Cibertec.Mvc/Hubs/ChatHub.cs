﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Cibertec.Mvc.Hubs
{
    public class MensajeChat
    {
        public string NombreUsuario { get; set; }
        public string Mensaje { get; set; }
    }
    public class ChatHub : Hub
    {
        // este listado almacena el histórico de mensajes (en un ambiente real, debería guardarse en una BD documental)
        static List<MensajeChat> Mensajes = new List<MensajeChat>();

        public void EnviarMensaje(string mensaje, string nombreUsuario)
        {
            Mensajes.Add(new MensajeChat { NombreUsuario = nombreUsuario, Mensaje = mensaje });
            Clients.All.actualizarMensajes(mensaje, nombreUsuario);
        }

        public override Task OnConnected()
        {
            // cuando un nuevo usuario se conecta al Hub, le decimos que incialice los mensajes guardados en el listado de Mensajes
            return Clients.Caller.inicializarMensajes(Mensajes);
        }
    }
}