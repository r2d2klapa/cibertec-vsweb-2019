﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cibertec.Mvc.Hubs
{
    public class ShapeHub : Hub
    {
        public void MoveShape(double x, double y)
        {
            // llamamos a los clientes para que actualicen la posición del cuadrado
            Clients.Others.updatePos(x, y);
        }
    }
}